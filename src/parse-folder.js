const Promise = require('bluebird')
const fs = require('fs')
const path = require('path')
const R = require('ramda')

const readDir = Promise.promisify(fs.readdir)
const readFile = Promise.promisify(fs.readFile)

function readFilenamesFromFolder (folderPath) {
  const filepath = path.join(process.cwd(), folderPath)
  const filenameRegExp = /^[0-9]+\.json$/
  return readDir(filepath)
    .then(R.filter(x => filenameRegExp.test(x))) // exclude .md files
}

function getUserId (filename) {
  const matchingResults = /(.*)\.json/.exec(filename)
  return matchingResults && matchingResults[1]
}

const processFile = folder => filename => {
  const filepath = path.join(process.cwd(), folder, filename)
  const userId = getUserId(filename)
  return parseFile(filepath)
    .then(jsonData => ({
      userId,
      meals: getMeals(jsonData)
    }))
}

// TODO still looks a bit over-complicated for the code does?
function getMeals (json) {
  const allMealsWithDetails = R.chain(
    R.values(R.__),
    R.map(
      R.path(['details', 'mealsWithDetails']),
      R.values(R.path(['calendar', 'daysWithDetails'])(json))
    )
  )
  const strDates = R.map(
    R.path(['details', 'date']),
    allMealsWithDetails
  )
  return strDates.map(strDate => ({
    date: new Date(strDate)
  }))
}

function parseFile (filepath) {
  return readFile(filepath)
    .then(JSON.parse)
}

// Read all JSON files in the folder
// and return an array of objects `{ userId, meals: [{ date }] }`
function parseFolder (folderPath) {
  return Promise.resolve(folderPath)
    .then(readFilenamesFromFolder)
    .then(files => Promise.map(
      files,
      processFile(folderPath), { concurrency: 100 }) // limit parallel requests
    )
}

module.exports = parseFolder
