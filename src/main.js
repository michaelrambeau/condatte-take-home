const R = require('ramda')

const userFilters = require('./user-filters')
const parseFolder = require('../src/parse-folder')

// Main function called by `/dist/run` shell file
// status can be either `active`, `superactive` or `bored`
function main ({ status, period, folder }) {
  const predicate = userFilters[status](period)
  const filterUsers = R.filter(predicate)
  const extractUserId = R.map(R.prop('userId'))
  const output = userIds => console.log(userIds.join(','))

  Promise.resolve(folder)
    .then(parseFolder)
    .then(filterUsers)
    .then(extractUserId)
    .then(output)
    .catch(console.error)
}

module.exports = main
