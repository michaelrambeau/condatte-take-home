const moment = require('moment')

// For a given period { startDate, endDate } return the previous period, of the same duration
function getPreviousPeriod (period) {
  const diff = moment(period.endDate).diff(
    moment(period.startDate),
    'days'
  )
  const duration = diff + 1
  const startDate = moment(period.startDate).subtract(duration, 'days').toDate()
  const endDate = moment(period.startDate).subtract(1, 'days').toDate()
  return {
    startDate,
    endDate
  }
}

module.exports = getPreviousPeriod
