const assert = require('assert')
const parseFolder = require('../src/parse-folder')
const DEBUG = false

// Parse a folder that contains only one small json file
parseFolder('test/data')
  .then(users => {
    assert(Array.isArray(users), 'The result should be an array')
    assert(users.length === 1, 'There should be 1 result')
    const user = users[0]
    assert(user.userId === '2627', 'The id should be `2627`')
    assert(Array.isArray(user.meals), 'There should an array `meals`')
    assert(user.meals.length > 10, 'There should items in the `meals` array')
    if (DEBUG) console.log('User found', user)
    return users
  })
  .catch(err => console.error(err))
