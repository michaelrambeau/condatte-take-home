const moment = require('moment')
const assert = require('assert')
const getPreviousPeriod = require('../src/previous-period')

const period = {
  startDate: new Date('2017-04-01'),
  endDate: new Date('2017-04-03')
}

const previousPeriod = getPreviousPeriod(period)

const formatDate = date => moment(date).format('YYYY-MM-DD')

assert(formatDate(previousPeriod.startDate) === '2017-03-29', 'incorrect startDate')
assert(formatDate(previousPeriod.endDate) === '2017-03-31', 'incorrect endDate')
