const getPreviousPeriod = require('./previous-period')

// Filter users who have created AT LEAST n meals during a given period
const hasCreatedMeals = count => period => user => {
  const meals = user.meals.filter(
    meal => meal.date >= period.startDate && meal.date <= period.endDate
  )
  return meals.length >= count
}

// Filter users who have NOT created AT LEAST n meals during a given period
// but who created AT lEAST those n meals during the following period
// TODO Could we use some curry stuff to remove the repetition of the parameters?
const hasChangedStatus = count => period => user => {
  const previousPeriod = getPreviousPeriod(period)
  return ((
    hasCreatedMeals(count)(previousPeriod)(user)
  ) && (
    !hasCreatedMeals(count)(period)(user)
  ))
}

// Export filter using keys that can be used from the command line
const filters = {
  active: hasCreatedMeals(5),
  superactive: hasCreatedMeals(10),
  bored: hasChangedStatus(5)
}

module.exports = filters
