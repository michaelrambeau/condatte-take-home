# Condatte "Take home"

## How to run it

This CLI tool is a basic node.js script, without any building process.

`./dist/run` can be run as an executable file from the command line.
You may need to adjust the permissions on the file (chmod -x run) but it may be simpler to call it using `node` program.

### Requirements

node.js > v.6.x and above must be installed on the machine.
(Caution: by default node 4.6 is installed on c9.io, use nvm to install the right version)

### Installation

Install the dependencies from npm:

```
npm install
```

### Commands


```
node ./dist/run active 2016-09-01 2016-09-08
```

```
node ./dist/run superactive 2016-09-01 2016-09-08
```

```
node ./dist/run bored 2016-09-01 2016-09-08
```


## Tests

Tests are simple node.js files located in the `test` folder, no testing framework is used, only basic `assert()` statements.
No output means all tests passed.

```
npm test
```

Tests run automatically on Gitlab CI after every push.
It's a way to check that the code is actually "cross-platform".

## How it works

The main algorithm, coded in `src/main.js` file, can be split  into 2 steps:

### STEP 1

Parse the `data` folder, reading the JSON files in parallel, and produce an array of users with the following structure:

```
[{ userId, meals: [{ date }] }]
```

### STEP 2

Filter the array of users with a single function that combines other functions.
Combining the building blocks is the fun part!

### Bonus

A cool feature: if you ever needed a new user status (for example the users who created at least 15 meals, let's call them the `hyperactive` users), all you have to do is add one line in the `src/user-filter.js` file:

```diff
const filters = {
  active: hasCreatedMeals(5),
  superactive: hasCreatedMeals(10),
+ hyperactive: hasCreatedMeals(15),
  bored: hasChangedStatus(5)
}
```

And then, automatically, you will be able to run this: `node ./dist/run hyperactive 2016-09-01 2016-09-08`

## Points to improve / TODO

- [x] Improve memory usage: the script reads all files in parallel, I think you can run out of memory if the folder contains thousands of big files. A maximum level of concurrency should be handled. DONE in 1.2 release!
- [ ] Improve performance: to deal with big files, it would be nicer to use node.js streams and pipe everything.
- [ ] Make the parsing script more robust, using a more functional approach (using `Maybe` to catch problems that could occur if some files had a non consistent structure)
- [ ] Add types (using either Flow or TypeScript?) in the mix... that would introduce a building process.
