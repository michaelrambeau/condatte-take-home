const moment = require('moment')
const userFilters = require('./user-filters')

// Parse CLI input parameters, passed from the command line
// Throw an error if incorrect
function checkInput ({ status, start, end }) {
  const allStatuses = Object.keys(userFilters) // ['active', 'superactive', 'bored']
  if (!allStatuses.includes(status)) throw new Error(`Invalid status ${status}`)

  if (!isValidDate(start)) throw new Error(`Invalid startDate: '${start}'`)
  if (!isValidDate(end)) throw new Error(`Invalid endDate: '${end}'`)
  const startDate = new Date(start)
  const endDate = new Date(end)

  return {
    status,
    period: {
      startDate,
      endDate
    }
  }
}

function isValidDate (strDate) {
  return moment(strDate, 'YYYY-MM-DD').isValid()
}

module.exports = checkInput
