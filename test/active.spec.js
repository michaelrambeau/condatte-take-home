const assert = require('assert')

const parseFolder = require('../src/parse-folder')
const filters = require('../src/user-filters')

parseFolder('test/data')
  .then(runTests)
  .catch(e => console.error(e))

const testCases = [
  {
    start: '2015-10-29', end: '2015-10-31',
    expected: { active: 1, superactive: 0, bored: 0 }
  },
  {
    start: '2015-10-30', end: '2015-10-31',
    expected: { active: 1, superactive: 0, bored: 0 }
  },
  {
    start: '2015-10-31', end: '2015-10-31',
    expected: { active: 0, superactive: 0, bored: 0 }
  },
  {
    start: '2015-11-01', end: '2015-11-06',
    expected: { active: 1, superactive: 0, bored: 0 }
  },
  {
    start: '2015-11-01', end: '2015-11-07',
    expected: { active: 1, superactive: 1, bored: 0 }
  },
  {
    start: '2015-11-01', end: '2015-11-31',
    expected: { active: 1, superactive: 1, bored: 0 }
  },
  {
    start: '2015-12-01', end: '2015-12-30',
    expected: { active: 0, superactive: 0, bored: 1 }
  }
]

function runTests (users) {
  testCases.forEach((testCase, i) => {
    const period = {
      startDate: new Date(testCase.start),
      endDate: new Date(testCase.end)
    }
    Object.keys(testCase.expected).map(key => {
      const predicate = filters[key](period)
      const result = users.filter(predicate).length
      assert(
        result === testCase.expected[key],
        `Use case #${i + 1}, incorrect result for status ${key}`
      )
    })
  })
}
